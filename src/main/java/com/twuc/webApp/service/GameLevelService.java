package com.twuc.webApp.service;

import com.twuc.webApp.domain.mazeGenerator.AldousBroderMazeAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.DijkstraSolvingAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.Grid;
import com.twuc.webApp.domain.mazeRender.MazeWriter;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

@Service
public class GameLevelService {
    private final List<MazeWriter> mazeWriters;
    private final AldousBroderMazeAlgorithm mazeAlgorithm;
    private final DijkstraSolvingAlgorithm solvingAlgorithm;

    public GameLevelService(List<MazeWriter> mazeWriters, AldousBroderMazeAlgorithm mazeAlgorithm, DijkstraSolvingAlgorithm solvingAlgorithm) {
        this.mazeWriters = mazeWriters;
        this.mazeAlgorithm = mazeAlgorithm;
        this.solvingAlgorithm = solvingAlgorithm;
    }

    public byte[] renderMaze(int width, int height, String type) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        renderMaze(stream, width, height, type);
        return stream.toByteArray();
    }

    public void renderMaze(OutputStream stream, int width, int height, String type) throws IOException {
        if (width <= 1) { throw new IllegalArgumentException(String.format("The width(%d) is not valid", width)); }
        if (height <= 1) { throw new IllegalArgumentException(String.format("The height(%d) is not valid", height)); }

        MazeWriter writer = mazeWriters
            .stream()
            .filter(w -> w.getName().equals(type))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Invalid type: " + type));
        Grid grid = new Grid(width, height);
        mazeAlgorithm.update(grid);
        solvingAlgorithm.update(grid);
        writer.render(grid, stream);
    }
}
